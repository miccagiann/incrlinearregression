package uot.dblab.incrlr;

import java.sql.*;
import java.util.TreeMap;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Collections;
import java.util.ListIterator;
import java.util.Map;

public class Optimizer {

    private double DIM; //The normal dimensionallity, not the one of augmented vectors.
    private double INIT_D; //The dimensionallity of the raw dataset.
    private double NREPR;
    private double PAGE_SIZE;
    private double PAGES_PER_BLOCK;
    private double L; //Number of vectors of dimensionality DIM+1 that fit into RAM.
    private static final String slctMVLRQ = "SELECT `id`, `start`, `range`, `matrix`, `vector` FROM `matrix_vector` WHERE `dataset_id` = ? AND `lr_id` = ?";
    private TreeMap<String,String> startpoints; //Endopoints -> Startpoints
    private TreeMap<String,String> endpoints;   //Startpoints -> Endpoints
    private TreeMap<Long,Structs.CachedInterval> cachedIntervals; //Cached Intervals...
    private TreeMap<Long,Structs.Penalty> penalty; //Initial penalty of using an interval...
    private Structs.policyNode exactSamePN;
    private long start; //Query Start
    private long end; //Query End

    public boolean OUTPUT;
   
    /**
     * Estimates the cost of accesing datapoints/records/tuples from relation 'data'.
     *
     * @param  n        The number of datapoints/tuples that we have to retrieve from relation 'data'.
     *
     * @return  The cost of accesing the raw datapoints from relation 'data' in order to produce matrix XXT and vector Xy.
     */
    public double CrCost(long n) {
        return Math.ceil( (this.INIT_D*n*this.NREPR)/(this.PAGE_SIZE*this.PAGES_PER_BLOCK) ) + 
            Math.max(0, 2*n*Math.ceil( ((this.INIT_D+1)*((this.INIT_D+1)-(this.L))*this.NREPR)/(this.PAGE_SIZE*this.PAGES_PER_BLOCK) ));
    }
    
    /**
     * Estimates the cost of accessing matrix XXT and vector Xy of an already estimated interval.
     *
     * @return  The cost of fetching matrix XXT and Xy vector.
     */
    public double FetchCost() {
        return Math.ceil( ((this.DIM+1)*this.NREPR)/(this.PAGE_SIZE*this.PAGES_PER_BLOCK) ) + Math.ceil( (Math.pow(this.DIM+1,2)*this.NREPR)/(this.PAGE_SIZE*this.PAGES_PER_BLOCK) );
    }

    /**
     * Initialize Optimizer with the specific execution parameters.
     *
     * @param  conn        Connection to current database.
     * @param  did         Dataset id.
     * @param  lr_id       Linear Regression id.
     * @param  start       Starting point of the desired query interval.
     * @param  range       Range of the desired query interval.
     * @param  initD       Dimensionality of the initial dataset.
     * @param  d           Dimensionality of data.
     * @param  f           Bytes for representing numerical values.
     * @param  p           Number of bytes that its page consists of.
     * @param  b           Number of pages belonging to each block.
     * @param  l           Number of vectors we can store in memory.
     *
     * @return  Boolean value indicating that the optimizer has been initialized successfully.
     *
     * @throws  SQLException, ClassNotFoundException
     */
    public boolean initializeOpt(Connection conn, long did, long lr_id, long start, long range, int initD, 
            int d, int f, int p, int b, int l) throws SQLException, ClassNotFoundException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        this.DIM = d;
        this.INIT_D = initD;
        this.NREPR = f;
        this.PAGE_SIZE = p;
        this.PAGES_PER_BLOCK = b;
        this.L = l;
        this.startpoints = new TreeMap<String,String>();
        this.endpoints = new TreeMap<String,String>();
        this.cachedIntervals = new TreeMap<Long,Structs.CachedInterval>();
        this.penalty = new TreeMap<Long,Structs.Penalty>();
        this.exactSamePN = null;
        this.start = start;
        this.end = this.start + range - 1;
        long currIID, currStart, currEnd, currRange;
        boolean success = false;
        StringBuffer startsbf = null;
        StringBuffer endsbf = null;
        double penaltyCost = 0.0;
        Structs.Penalty tempPenalty = new Structs.Penalty(0.0,0,0,0);
        Structs.policyNode tmpPolNode = new Structs.policyNode(0,0,0,Structs.Action.create);
        Structs.CachedInterval tmpCachedIntv = new Structs.CachedInterval();

        //Connect to the database and query relation 'matrix_vector' multiplication so as to estimate the best execution plan
        //using appropriate already estimated intervals.
        try {
            //Before accessing the database for already estimated intervals add the start and end points of the query
            //to startpoins and endpoints maps with interval id equal to 0.
            long startTime = System.nanoTime();
            this.startpoints.put(end+"_0", start+"_0");
            this.endpoints.put(start+"_0", end+"_0");
            Class.forName("com.mysql.jdbc.Driver");
            pstmt = conn.prepareStatement(this.slctMVLRQ);
            pstmt.setLong(1, did); pstmt.setLong(2, lr_id);
            rs = pstmt.executeQuery();
            startsbf = new StringBuffer();
            endsbf = new StringBuffer();
            while (rs.next()) {
                currIID =  rs.getLong(1); currStart = rs.getLong(2); currRange = rs.getLong(3); currEnd = currStart + currRange - 1;
                if (currStart < end && currEnd > start) { // We have found an interval that overlaps with our query.
                    tmpCachedIntv.XXT = new byte[(d+1)*(d+1)*f]; tmpCachedIntv.Xy = new byte[(d+1)*f];
                    System.arraycopy(rs.getBlob(4).getBytes(1, f*(d+1)*(d+1)), 0, tmpCachedIntv.XXT, 0, (d+1)*(d+1)*f);
                    System.arraycopy(rs.getBlob(5).getBytes(1, f*(d+1)), 0, tmpCachedIntv.Xy, 0, (d+1)*f);
                    if (currStart >= start && currEnd <= end) { //Interval is totally overlapped by user's query -> No need to penalize it...
                        if (currStart == start && currEnd == end) {
                            this.exactSamePN = new Structs.policyNode(currIID, currStart, currEnd, Structs.Action.fetch);
                            break; //We have found the exact same interval in our database... We do not need to do anything else.
                        }
                        startsbf.append(currStart); startsbf.append("_"); startsbf.append(currIID);
                        endsbf.append(currEnd); endsbf.append("_"); endsbf.append(currIID);
                    } else if (currStart < start && currEnd <= end) { //Interval starts before user's query...
                        startsbf.append(start); startsbf.append("_"); startsbf.append(currIID);
                        endsbf.append(currEnd); endsbf.append("_"); endsbf.append(currIID);
                        tmpPolNode.iid = 0; tmpPolNode.start = currStart; tmpPolNode.end = start - 1; tmpPolNode.action = Structs.Action.subtract;
                        tempPenalty.cost = this.CrCost(start - currStart);
                        tempPenalty.iid = currIID; tempPenalty.start = currStart; tempPenalty.end = currEnd;
                        tempPenalty.policyNodes.add(tmpPolNode.copy());
                        this.penalty.put(currIID, tempPenalty.copy());
                    } else if (currStart >= start && currEnd > end) { //Interval ends after user's query...
                        startsbf.append(currStart); startsbf.append("_"); startsbf.append(currIID);
                        endsbf.append(end); endsbf.append("_"); endsbf.append(currIID);
                        tmpPolNode.iid = 0; tmpPolNode.start = end + 1; tmpPolNode.end = currEnd; tmpPolNode.action = Structs.Action.subtract;
                        tempPenalty.cost = this.CrCost(currEnd - end);
                        tempPenalty.iid = currIID; tempPenalty.start = currStart; tempPenalty.end = currEnd;
                        tempPenalty.policyNodes.add(tmpPolNode.copy());
                        this.penalty.put(currIID, tempPenalty.copy());
                    } else { //Interval totally overlaps user's query...
                        startsbf.append(start); startsbf.append("_"); startsbf.append(currIID);
                        endsbf.append(end); endsbf.append("_"); endsbf.append(currIID);
                        //First penalize the start point.
                        tmpPolNode.iid = 0; tmpPolNode.start = currStart; tmpPolNode.end = start - 1; tmpPolNode.action = Structs.Action.subtract;
                        tempPenalty.cost = this.CrCost(start - currStart);
                        tempPenalty.iid = currIID; tempPenalty.start = currStart; tempPenalty.end = currEnd;
                        tempPenalty.policyNodes.add(tmpPolNode.copy());
                        //Then penalize the end point.
                        tmpPolNode.iid = 0; tmpPolNode.start = end + 1; tmpPolNode.end = currEnd; tmpPolNode.action = Structs.Action.subtract;
                        tempPenalty.cost += this.CrCost(currEnd - end);
                        tempPenalty.policyNodes.add(tmpPolNode.copy());
                        //Finally keep track of the initial penalty whenever this interval is involved.
                        this.penalty.put(currIID, tempPenalty.copy());
                    }
                    //Insert starting and ending point of current intervals to the TreeMaps.
                    this.cachedIntervals.put(currIID, tmpCachedIntv.copy()); //Cache this interval...
                    this.startpoints.put(endsbf.toString(), startsbf.toString());
                    this.endpoints.put(startsbf.toString(), endsbf.toString());
                    tempPenalty.policyNodes.clear();
                    tempPenalty.cost = 0;
                    startsbf.setLength(0);
                    endsbf.setLength(0);
                }
            }
            System.out.printf("SEEK MV: %.4f\n", (System.nanoTime() - startTime)/Math.pow(10,9.0));
            success = true;
        } finally {
            if (rs != null) { 
                rs.close();
                rs = null;
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        }
        //DEBUG INFO:
        /*System.out.println("Contents of listStart:");
        for (String STR: this.startpoints.keySet())
            System.out.printf("%s ,", STR);
        System.out.println("");
        System.out.println("Contents of listEnd:");
        for (String STR: this.endpoints.keySet())
            System.out.printf("%s ,", STR);
        System.out.println(""); 
        System.out.println("Contents of penalty:");
        for (Map.Entry<Long,Structs.Penalty> STR: this.penalty.entrySet())
            System.out.printf("%d --> %s\n", STR.getKey(), STR.getValue().toString());
        return true; */
        //
        return success;
    }

    /**
     * Finds the best way to combine already estimated intervals so as to produce a subinterval with the minimum possible cost.
     *
     * @param  a            Point a.
     * @param  b            Point b.
     * @param  p            Temp Policy.
     * @param  c            Current case.
     * @param  dummy        dummy policyNode object so as to avoid creating new objects.
     * @param  updtPolicy   Boolean parameter that denotes if we should update Temp Policy ArrayList.
     *
     * @return  Optimal Cost for producing this subinterval as well as the new temp policy 'p'.
     */
    private double bestCost(String a, String b, ArrayList<Structs.policyNode> p, Structs.Case c, Structs.policyNode dummy, boolean updtPolicy) {
        double currCost = 0.0;
        double singleCost = 0.0;
        double comboCost = 0.0;
        long iid = -1; long s = 0; long e = 0; long n = 0;
        Structs.Penalty initPenalty;
        p.clear();
        switch(c) {
            case endstart: //Open - Open
                e = Structs.Point.dismantleKey(b); s = Structs.Point.dismantleKey(a);
                if (e - s == 1) { //Consecutive intervals --> Safely ignore this case
                    currCost = 0;
                    break;
                }
                if (Structs.Point.extractIID(a) == 0) //Case where the endpoint is the initial start point...
                    n = e - s;
                else
                    n = e - s - 1;
                currCost = this.CrCost(n);
                if (updtPolicy) {
                    dummy.end = e - 1; dummy.iid = 0; dummy.action = Structs.Action.create;
                    if (Structs.Point.extractIID(a) == 0) //Case where the endpoint is the initial start point...
                        dummy.start = s;
                    else
                        dummy.start = s + 1;
                    p.add(dummy.copy());
                }
                break;
            case startstart: //Close - Open
                initPenalty = this.penalty.get(Structs.Point.extractIID(a));
                String endpoint = this.endpoints.get(a);
                n = Structs.Point.dismantleKey(b) - Structs.Point.dismantleKey(a);
                singleCost = CrCost(n); 
                n = Structs.Point.dismantleKey(endpoint) - Structs.Point.dismantleKey(b) + 1;
                comboCost = this.CrCost(n) + this.FetchCost();
                if (initPenalty != null)
                    comboCost += initPenalty.cost;
                if (singleCost <= comboCost) {
                    currCost = singleCost;
                    if (updtPolicy) {
                        dummy.iid = 0;
                        dummy.start = Structs.Point.dismantleKey(a); dummy.end = Structs.Point.dismantleKey(b) - 1; dummy.action = Structs.Action.create;
                        p.add(dummy.copy());
                    }
                } else {
                    currCost = comboCost;
                    if (updtPolicy) {
                        dummy.iid = Structs.Point.extractIID(a);
                        if (initPenalty != null) {
                            dummy.start = initPenalty.start; 
                            dummy.end = initPenalty.end;
                        } else {
                            dummy.start = Structs.Point.dismantleKey(a); 
                            dummy.end = Structs.Point.dismantleKey(endpoint);
                        }
                        dummy.action = Structs.Action.fetch;
                        p.add(dummy.copy());
                        dummy.iid = 0;
                        dummy.start = Structs.Point.dismantleKey(b); 
                        dummy.end = Structs.Point.dismantleKey(endpoint); 
                        dummy.action = Structs.Action.subtract;
                        p.add(dummy.copy());
                        if (initPenalty != null) {
                            p.addAll(initPenalty.policyNodes);
                        }
                    }
                }
                break;
            case startend: //Close - Close
                initPenalty = this.penalty.get(Structs.Point.extractIID(a));
                n = Structs.Point.dismantleKey(b) - Structs.Point.dismantleKey(a) + 1;
                singleCost = this.CrCost(n);
                comboCost = this.FetchCost();
                if (initPenalty != null)
                    comboCost += initPenalty.cost;
                if (singleCost <= comboCost || Structs.Point.extractIID(a) == 0) { //Also case dealing with the start of the Q.
                    currCost = singleCost;
                    if (updtPolicy) {
                        dummy.iid = 0; 
                        dummy.start = Structs.Point.dismantleKey(a); dummy.end = Structs.Point.dismantleKey(b); dummy.action = Structs.Action.create;
                        p.add(dummy.copy());
                    }
                } else {
                    currCost = comboCost;
                    if (updtPolicy) {
                        if (initPenalty != null) {
                            dummy.iid = initPenalty.iid;
                            dummy.start = initPenalty.start;
                            dummy.end = initPenalty.end; 
                            dummy.action = Structs.Action.fetch;
                            p.add(dummy.copy());
                            p.addAll(initPenalty.policyNodes);
                        } else {
                            dummy.iid = Structs.Point.extractIID(a); 
                            dummy.start = Structs.Point.dismantleKey(a); dummy.end = Structs.Point.dismantleKey(b); dummy.action = Structs.Action.fetch;
                            p.add(dummy.copy());
                        }
                    }
                }
                break;
            case endend: //Open - Close
                String startpoint = startpoints.get(b); 
                if (Structs.Point.extractIID(startpoint) == 0) { //We are in the query point... We need to have only create cost.
                    n = Structs.Point.dismantleKey(b) - Structs.Point.dismantleKey(a);
                    currCost = CrCost(n);
                    if (updtPolicy) {
                        dummy.iid = 0;
                        dummy.start = Structs.Point.dismantleKey(a) + 1; dummy.end = Structs.Point.dismantleKey(b); dummy.action = Structs.Action.create;
                        p.add(dummy.copy());
                    }
                } else {
                    initPenalty = this.penalty.get(Structs.Point.extractIID(startpoint));
                    n = Structs.Point.dismantleKey(b) - Structs.Point.dismantleKey(a);
                    singleCost = CrCost(n);
                    n = Structs.Point.dismantleKey(a) - Structs.Point.dismantleKey(startpoint) + 1;
                    comboCost = CrCost(n) + this.FetchCost();
                    if (initPenalty != null)
                        comboCost += initPenalty.cost;
                    if (singleCost <= comboCost) {
                        currCost = singleCost;
                        if (updtPolicy) {
                            dummy.iid = 0;
                            dummy.start = Structs.Point.dismantleKey(a) + 1; dummy.end = Structs.Point.dismantleKey(b); dummy.action = Structs.Action.create;
                            p.add(dummy.copy());
                        }
                    } else {
                        currCost = comboCost;
                        if (updtPolicy) {
                            dummy.iid = Structs.Point.extractIID(b);
                            if (initPenalty != null) {
                                dummy.start = initPenalty.start; 
                                dummy.end = initPenalty.end;
                            } else {
                                dummy.start = Structs.Point.dismantleKey(startpoint);
                                dummy.end = Structs.Point.dismantleKey(b); 
                            }
                            dummy.action = Structs.Action.fetch;
                            p.add(dummy.copy());
                            dummy.iid = 0;
                            dummy.start = Structs.Point.dismantleKey(startpoint); dummy.end = Structs.Point.dismantleKey(a); dummy.action = Structs.Action.subtract;
                            p.add(dummy.copy());
                            if (initPenalty != null) {
                                p.addAll(initPenalty.policyNodes);
                            }
                        }
                    }
                }
                break;
        }
        return currCost; 
    }

    /**
     * Estimates the plan that produces the optimal cost for applying Linear Regression operator in the user's query interval.
     *
     * @return  A struct consisting of the all the intervals that we need either to create or to fetch from relation 'matrix_vector' as 
     *          well as the cost of executing this plan.
     */
    public Structs.CostPlan generatePlan() {
        TreeMap<Long,Structs.CostPlan> plan = new TreeMap<Long,Structs.CostPlan>();
        LinkedList<Structs.Point> points = new LinkedList<Structs.Point>();
        LinkedList<Structs.Point> listStart = new LinkedList<Structs.Point>();
        LinkedList<Structs.Point> listEnd = new LinkedList<Structs.Point>();
        double tempCost = 0.0;
        double best = 0.0;
        boolean updateCostPolicy;
        String intervalStart;
        ArrayList<Structs.policyNode> tempPolicy = new ArrayList<Structs.policyNode>();
        Structs.policyNode dummy = new Structs.policyNode(0,0,0,Structs.Action.create); //Dummy policyNode object
        Structs.CostPlan queryPlan = new Structs.CostPlan(0.0); //Used for dummy purposes
        Structs.Point tempPoint = new Structs.Point("0",true); //Point used for traversing through lists 'listStart' and 'listEnd'
        Structs.Point PTBErased = new Structs.Point("0",true); //Dummy point used to denote the element we want to erase from a list
        Structs.Point bestTempPoint = null;
        boolean caseStartEnd;
        Structs.CostPlan tmpCP = new Structs.CostPlan(0.0);
        
        // Initialization
        for (String k: this.endpoints.keySet())
            points.add(new Structs.Point(k, true));
        for (String k: this.startpoints.keySet())
            points.add(new Structs.Point(k, false));
        Collections.sort(points);
        //Printing all the points:
        /*for (Structs.Point STR: points) {
            System.out.printf("%s, ", STR.label);
        }
        System.out.printf("\n");*/
        Structs.Point currPoint = points.removeFirst();
        Structs.Point endPoint = currPoint;
        plan.put(Structs.Point.dismantleKey(currPoint.label), queryPlan.copy());
        //listEnd.add(currPoint);   

        // While loop for all start and end points of intervals.
        while (!points.isEmpty()) {
            currPoint = points.removeFirst();
            tempPolicy.clear();
            bestTempPoint = null;
            updateCostPolicy = true;
            if (currPoint.isStartpoint) { // Visited point is a start point of an interval...
                if (plan.get(Structs.Point.dismantleKey(currPoint.label)) == null) { // We have not an optimal plan for reaching this point so far...
                    queryPlan = plan.get(Structs.Point.dismantleKey(endPoint.label));
                    tempCost = queryPlan.cost + this.bestCost(endPoint.label, currPoint.label, tempPolicy, Structs.Case.endstart, dummy, false);
                    for (ListIterator<Structs.Point> it = listStart.listIterator(); it.hasNext(); ) {
                        tempPoint = it.next();
                        queryPlan = plan.get(Structs.Point.dismantleKey(tempPoint.label));
                        best = bestCost(tempPoint.label, currPoint.label, tempPolicy, Structs.Case.startstart, dummy, false);
                        if (tempCost > queryPlan.cost + best) {
                            tempCost = queryPlan.cost + best;
                            bestTempPoint = tempPoint;
                        }
                    }
                    // TIME TO UPDATE POLICY...
                    if (bestTempPoint != null) { //Optimal cost derives from startstart policy...
                        bestCost(bestTempPoint.label, currPoint.label, tempPolicy, Structs.Case.startstart, dummy, true);
                        queryPlan = plan.get(Structs.Point.dismantleKey(bestTempPoint.label));
                    } else { //Optimal cost derives from endstart policy...
                        bestCost(endPoint.label, currPoint.label, tempPolicy, Structs.Case.endstart, dummy, true);
                        queryPlan = plan.get(Structs.Point.dismantleKey(endPoint.label));
                    }
                    tempPolicy.addAll(queryPlan.plan);
                }
                listStart.add(currPoint);
            } else { // Visited point is an end point of an interval...
                caseStartEnd = false;
                intervalStart = startpoints.get(currPoint.label);
                queryPlan = plan.get(Structs.Point.dismantleKey(currPoint.label));
                if (queryPlan == null) { //no previous cost for reaching this point.
                    tempCost = Double.POSITIVE_INFINITY;
                    bestTempPoint = null;
                } else { 
                    tempCost = queryPlan.cost;
                } 
                queryPlan = plan.get(Structs.Point.dismantleKey(intervalStart));
                best = this.bestCost(intervalStart, currPoint.label, tempPolicy, Structs.Case.startend, dummy, false);
                    //DEBUG
                    /*System.out.println("Comparison Cost: " + (queryPlan.cost + best));*/
                if (tempCost >  queryPlan.cost + best) {
                    tempCost = queryPlan.cost + best;
                    caseStartEnd = true;
                }
                PTBErased.label = intervalStart; PTBErased.isStartpoint = true;
                listStart.remove(PTBErased);
                for (ListIterator<Structs.Point> it = listEnd.listIterator(); it.hasNext(); ) {
                    tempPoint = it.next();
                    //DEBUG
                    /*System.out.println("Compare Point: " + tempPoint.label);*/
                    if (Structs.Point.dismantleKey(tempPoint.label) < Structs.Point.dismantleKey(intervalStart))
                        it.remove();
                    else {
                        queryPlan = plan.get(Structs.Point.dismantleKey(tempPoint.label));
                        best = this.bestCost(tempPoint.label, currPoint.label, tempPolicy, Structs.Case.endend, dummy, false);
                        if (tempCost > queryPlan.cost + best) {
                            tempCost = queryPlan.cost + best;
                            bestTempPoint = tempPoint;
                            caseStartEnd = false;
                        }
                    }
                }
                //TIME TO UPDATE POLICY
                if (caseStartEnd) {
                    this.bestCost(intervalStart, currPoint.label, tempPolicy, Structs.Case.startend, dummy, true);
                    queryPlan = plan.get(Structs.Point.dismantleKey(intervalStart));
                    tempPolicy.addAll(queryPlan.plan);
                } else if (bestTempPoint != null && bestTempPoint.label.compareTo(currPoint.label) != 0) {
                    queryPlan = plan.get(Structs.Point.dismantleKey(bestTempPoint.label));
                    this.bestCost(bestTempPoint.label, currPoint.label, tempPolicy, Structs.Case.endend, dummy, true);
                    tempPolicy.addAll(queryPlan.plan);
                } else 
                    updateCostPolicy = false;
                // Clear listStart in cases we are allowed...
                if (listStart.isEmpty())
                    listEnd.clear();
                listEnd.add(currPoint);
                endPoint = currPoint;
            }
            //DEBUG
            /*System.out.println("Contents of listStart:");
            for (Structs.Point STR: listStart)
                System.out.printf("%s ,", STR.label);
            System.out.println("");
            System.out.println("Contents of listEnd:");
            for (Structs.Point STR: listEnd)
                System.out.printf("%s ,", STR.label);
            System.out.printf("\n");
            System.out.println("Endpoint: " + endPoint.label);
            */
            if (updateCostPolicy) {
                tmpCP.deepCopy(tempCost, tempPolicy);
                //DEBUG 
                /*System.out.printf("%s ==::==  ", currPoint.label);
                System.out.println(tmpCP);*/
                //
                plan.put(Structs.Point.dismantleKey(currPoint.label), tmpCP.copy());
            }
            /*System.out.println("");*/ 
            System.gc();
        }
        //finally estimate the cost of producing Linear Regression from scratch...
        tempCost = this.CrCost(this.end - this.start + 1);
        //DEBUG INFO
        //System.out.printf("Scratch Cost: %.2f vs Combo Cost: %.2f\n", tempCost, plan.get(plan.lastKey()).cost);
        //
        /*if (tempCost <= plan.get(plan.lastKey()).cost 
                || plan.get(plan.lastKey()).plan.isEmpty()) {
            dummy.iid = 0; 
            dummy.start = this.start;
            dummy.end = this.end;
            dummy.action = Structs.Action.create;
            tmpCP.plan.clear();
            tmpCP.cost = tempCost;
            tmpCP.plan.add(dummy);
            return tmpCP;
        } else { */
        return plan.get(plan.lastKey());
        //return (plan.lastKey() == null) ? null : plan.get(plan.lastKey());
        //}
    }

    /**
     * Update the policy nodes of the plan with the cached information for matrix XXT and vector Xy.
     */
    private void updatePlan(ArrayList<Structs.policyNode> plan) {
        Structs.policyNode curr = null;
        Structs.CachedInterval cintv = null;
        ListIterator<Structs.policyNode> it = plan.listIterator();
        while(it.hasNext()) {
            curr = it.next();
            if (curr.action == Structs.Action.fetch) {
                curr.cached = true;
                cintv = this.cachedIntervals.get(curr.iid);
                curr.XXT = cintv.XXT;
                curr.Xy = cintv.Xy;
            }
        }
    }

    /**
     * Concatenate consecutive intervals so as to reduce the number of the queries.
     */
    private void concatenateIntervals(Structs.CostPlan cp) {
        Collections.sort(cp.plan);
        ListIterator<Structs.policyNode> it = cp.plan.listIterator();
        Structs.policyNode currNode = null;
        Structs.policyNode nextNode = null;
        if (it.hasNext())
            currNode = it.next();
        while (it.hasNext()) {
            nextNode = it.next();
            if (currNode.action == Structs.Action.create
                && nextNode.action == Structs.Action.create
                && currNode.end + 1 == nextNode.start) {
                
                currNode.end = nextNode.end;
                it.remove();
            }
        }
    }

    /**
     * Closes the optimizer.
     */
    private void closeOptimizer() {
        this.DIM = 0;
        this.INIT_D = 0;
        this.NREPR = 0;
        this.PAGE_SIZE = 0;
        this.PAGES_PER_BLOCK = 0;
        this.L = 0;
        this.start = 0;
        this.end = 0;
        this.startpoints.clear();
        this.endpoints.clear();
        this.penalty.clear();
        this.cachedIntervals.clear();
        this.startpoints = null;
        this.endpoints = null;
        this.penalty = null;
        this.exactSamePN = null;
        this.cachedIntervals = null;
        System.gc();
    }

    /**
     * Executes the optimizer in order to find the best possible execution plan for 
     * estimating the Linear Regression.
     *
     * @param  conn      Connection with current database.
     * @param  did       The dataset's id.
     * @param  lr_id     Linear Regression id.
     * @param  start     Start point of user's query.
     * @param  range     Range of user's query.
     * @param  initD     Dimensionallity of the initial dataset.
     * @param  d         Number of dimensions (not the dimensionallity of augmented vectors).
     */
    public ArrayList<Structs.policyNode> execute(Connection conn, long did, long lr_id, long start, long range, int initD, int d) {
        Structs.CostPlan cp = null;
        try {
            if (this.initializeOpt(conn, did, lr_id, start, range, initD, d, 8, 4096, 1, d)
                    && this.exactSamePN == null) {
                long startTime = System.nanoTime();
                cp = this.generatePlan();
                this.updatePlan(cp.plan);
                this.concatenateIntervals(cp);
                long endTime = System.nanoTime();
                //if (cp != null) {
                    if (this.OUTPUT) { 
                        long totalPoints = 0;
                        System.out.printf("OPTIMIZER EXE TIME: %.2f\n", (endTime-startTime)/Math.pow(10, 9.0));
                        System.out.printf("Execution Plan Cost: %.2f\n", cp.cost);
                        if (cp.plan.size() >= 1) { 
                            System.out.println("Optimizer terminated successfully!");
                            System.out.println("Generated Plan: ");
                            for (int i = 0; i < cp.plan.size(); i++) {
                                System.out.printf("[%d,%d]: id=%d, action=", cp.plan.get(i).start, cp.plan.get(i).end, cp.plan.get(i).iid);
                                switch(cp.plan.get(i).action) {
                                    case create:
                                        System.out.printf("create | ");
                                        break;
                                    case subtract:
                                        totalPoints -= (cp.plan.get(i).end - cp.plan.get(i).start + 1);
                                        System.out.printf("subtract | ");
                                        break;
                                    case fetch:
                                        totalPoints += (cp.plan.get(i).end - cp.plan.get(i).start + 1);
                                        System.out.printf("fetch | ");
                                        break;
                                }
                            }
                            System.out.printf("\n");
                            System.out.printf("INTERVAL COVERAGE: %.2f%%\n", (totalPoints*100.0)/range);
                            System.out.printf("TOTAL COVERED POINTS: %d\n", totalPoints);
                        }
                    }
                //}
            } else { //This is needed for fast retrieved intervals that are exactly the same as the user's query.
                ArrayList<Structs.policyNode> simplePlan = new ArrayList<Structs.policyNode>();
                simplePlan.add(this.exactSamePN);
                return simplePlan;
            } 
        } catch (SQLException e) {
            System.err.println("Optimizer failed!");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.err.println("Optimizer failed!");
            e.printStackTrace();
        } finally {
            this.closeOptimizer();
            System.gc();
        }
        //TODO: Verify that this condition is not needed... [Verified -- However leaving this statement until many different executions to be performed]
        //return (cp != null) ? cp.plan : null;
        //Before returning the plan make sure that you concatenate multiple intervals.
        return cp.plan;
    }
}
//}
