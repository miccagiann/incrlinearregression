package uot.dblab.incrlr;

import java.util.ArrayList;

public class Structs {

    public enum Action { 
        create, subtract, fetch
    }

    public enum Case{ 
        startstart, startend, endstart, endend
    }

    public static class featvalueMap {
        public String feature;
        public int valueId;

        public featvalueMap(String f, int vid) {
            this.feature = f;
            this.valueId = vid;
        }
    }

    public static class policyNode implements Comparable<policyNode> {
        public long iid; //When it is equal to zero, it means that no already estimated interval exists for this range of points.
        public long start;
        public long end;
        public byte[] XXT;
        public byte[] Xy;
        public boolean cached;
        public Action action;

        public policyNode(long s, long e) {
            this(0,s,e,Action.create);
        }

        public policyNode(long id, long s, long e, Action a) {
            this.iid = id;
            this.start = s;
            this.end = e;
            this.action = a;
            this.cached = false;
            this.XXT = null;
            this.Xy = null;
        }

        public policyNode copy() {
            return new policyNode(this.iid, this.start, this.end, this.action);
        }
        
        @Override
        public int compareTo(policyNode pn) {
            if (this.start < pn.start) return -1;
            else if (this.start > pn.start) return 1;
            else {
                if (this.end < pn.end) return -1;
                else if (this.end > pn.end) return 1;
                return 0;
            }
        }

        @Override
        public String toString() {
            return ("[iid: " + this.iid + ", start: " + this.start + ", end: " + this.end + ", action: " + this.action + ", cached: " + this.cached + "]");
        }
    }

    public static class CachedInterval {
        public byte[] XXT;
        public byte[] Xy;

        public CachedInterval() {
            this.XXT = null;
            this.Xy = null;
        }

        public CachedInterval copy() {
            CachedInterval newObject = new CachedInterval();
            newObject.XXT = this.XXT;
            newObject.Xy = this.Xy;
            return newObject;
        }
    }

    public static class Penalty {
        public double cost;
        public long iid;
        public long start;
        public long end;
        public ArrayList<policyNode> policyNodes;

        public Penalty(double cost, long id, long s, long e) {
            this.cost = cost;
            this.iid = id;
            this.start = s;
            this.end = e;
            this.policyNodes = new ArrayList<policyNode>();
        }

        public Penalty copy() {
            Penalty newObj = new Penalty(this.cost, this.iid, this.start, this.end);
            newObj.policyNodes.addAll(this.policyNodes);
            return newObj;
        }

        @Override
        public String toString() {
            String str = "";
            for (int i = 0; i < policyNodes.size(); i++) {
                str += policyNodes.get(i).toString();
                str += ",  ";
            }
            return str;
        }
    }

    public static class Point implements Comparable<Point> {

        public String label;
        public boolean isStartpoint;

        public Point(String l, boolean startpoint) {
            this.label = l;
            this.isStartpoint = startpoint;
        }

        public static Long dismantleKey(String key) {
            return Long.parseLong(key.substring(0,key.indexOf('_')));
        } 
    
        public static Long extractIID(String key) {
            return Long.parseLong(key.substring(key.indexOf('_') + 1));
        }

        @Override
        public int compareTo(Point p2) {
            long v1 = this.dismantleKey(this.label);
            long v2 = this.dismantleKey(p2.label);
            if (v1 < v2) return -1;
            else if (v1 > v2) return 1;
            else {
                long iid1 = this.extractIID(this.label);
                long iid2 = this.extractIID(p2.label);
                if (iid1 < iid2) return -1;
                else if (iid1 > iid2) return 1;
                return 0;
            }
        }

        @Override
        public boolean equals(Object other) {
            if (other == null) return false;
            if (other == this) return true;
            if (!(other instanceof Point)) return false;
            Point otherPoint = (Point)other;
            if (this.label.compareTo(otherPoint.label) == 0 && this.isStartpoint == otherPoint.isStartpoint)
                return true;
            else
                return false;
        }
    }

    public static class CostPlan {
        
        public double cost;
        public ArrayList<policyNode> plan;

        public CostPlan(double c) {
            this.cost = c;
            this.plan = new ArrayList<policyNode>();
        }

        public void deepCopy(double c, ArrayList<policyNode> p) {
            this.plan.clear();
            this.cost = c;
            this.plan.addAll(p);
        }

        public CostPlan copy() {
            CostPlan newObj = new CostPlan(this.cost);
            newObj.plan.addAll(this.plan);
            return newObj;
        }

        @Override
        public String toString() {
            String str = "[Cost: " + cost + "-> ";
            for (Structs.policyNode pn: this.plan) {
                str += pn.toString();
            }
            return str;
        }
    }
}
