package uot.dblab.incrlr;

import org.jblas.*;
import java.sql.*;
import java.util.ArrayList;
import java.nio.ByteBuffer;

/**
 * Class that is responsible for executing linear regression upon a dataset already stored
 * in the backend. It can either perform linear regression on the datapoints of the dataset
 * from scratch or leverage the already estimated linear regressions applied to the respective
 * dataset.
 */
public class LinearRegression {

    private long lrid;
    private static final int SIZE_OF_DOUBLE = 8;
    private static final String insrtLRQ = "INSERT INTO `linearregres` (`dataset_id`, `d`, `features`, `target`) VALUES (?,?,?,?)";
    private static final String insrtMVQ = "INSERT INTO `matrix_vector` (`dataset_id`, `lr_id`, `start`, `range`, `matrix`, `vector`, `coeffs`) " 
        + "VALUES (?,?,?,?,?,?,?)";
    private static final String slctDataQ = "SELECT `values` FROM `data` WHERE `timestamp` >= ? AND `timestamp` <= ? AND `dataset_id` = ?"; 
    private static final String slctLRQ = "SELECT `id`, `features` FROM `linearregres` WHERE `dataset_id` = ? AND `target` = ?";
    private static final String slctMVQ = "SELECT `matrix`, `vector` FROM `matrix_vector` WHERE `id` = ?";
    private static final String slctMVQ_coeff = "SELECT `coeffs` FROM `matrix_vector` WHERE `id` = ?";
    private DoubleMatrix XXT; //(d+1)x(d+1)
    private DoubleMatrix Xy;  //(d+1)x1
    private DoubleMatrix X;   // nxd
    private DoubleMatrix y;   // nx1
    private DoubleMatrix w;   //(d+1)x1
    private int rows;
    private ArrayList<Structs.policyNode> plan;
    private boolean finished; //indicates the state of Linear Regression execution.
    private boolean exactlySame;

    public boolean OUTPUT;

    /**
     * Returns the id of the current Logistic Regression we are executing.
     */
    public long getLRID() {
        return this.lrid;
    }

    /**
     * It is called everytime before the execution of Linear Regression in order to
     * estimate the most suitable sizes for initializing matrices XXT, Xy, X, y.
     *
     * @param  conn         Connection with the database.
     * @param  did          Dataset id.
     * @param  initD        Dimensionality of initial dataset.
     * @param  d            The number of features upon which we perform LR on the current dataset.
     * @param  start        Start point of Linear Regression query.
     * @param  range        The range of user's query.
     * @param  opt          Boolean parameter that it is 'true' when planner suggests
     *                      combining already existing Linear Regression instances.
     */
    private boolean initializeLR(Connection conn, long did, int initD, int d, long start, long range, boolean opt) {
        this.finished = false;
        this.exactlySame = false;
        long startTime = 0, ellapsedTime = 0;
        /*long freeMem = Runtime.getRuntime().freeMemory();
        double numOfEls = Math.floor((freeMem*1.0/SIZE_OF_DOUBLE)*0.85);
        double reqEls = 4*Math.pow((d+1), 2.0) + 4*(d+1); //have more conservative policy...
        if (reqEls >= numOfEls) {
            System.err.println("Warning: Free Memory is less than expected!");
            return false;
        }
        double remEls = numOfEls - reqEls;
        this.rows = (int)Math.floor(remEls/(4*(d+1.0)));
        System.out.println("Available Rows: " + rows);
        if (rows <= 1) {
            System.err.println("Error: Performing LR needs more memory!");
            return false;
        }*/
        startTime = System.nanoTime();
        this.rows = 1000000;
        this.X = DoubleMatrix.zeros(this.rows, d+1);
        this.y = DoubleMatrix.zeros(this.rows, 1);
        this.XXT = DoubleMatrix.zeros(d+1, d+1);
        this.Xy = DoubleMatrix.zeros(d+1, 1);
        ellapsedTime += (System.nanoTime() - startTime);
        if (opt && this.lrid != 0) { //using existing LR models is more beneficial
            //Call the optimizer...
            startTime = System.nanoTime();
            Optimizer optimizer = new Optimizer();
            ellapsedTime += (System.nanoTime() - startTime);
            optimizer.OUTPUT = this.OUTPUT;
            this.plan = optimizer.execute(conn, did, this.lrid, start, range, initD, d);
            if (this.plan != null && this.plan.size() == 1 
                    && this.plan.get(0).start == start && this.plan.get(0).end == start + range - 1
                    && this.plan.get(0).action == Structs.Action.fetch)
                this.exactlySame = true; // The optimizer has found the exact same Linear Regression Query having been executed.
        } else { //performing LR from scratch is more beneficial 
            this.plan = new ArrayList<Structs.policyNode>();
            this.plan.add(new Structs.policyNode(0, start, start+range-1, Structs.Action.create));
        }
        /*if (this.exactlySame)
            System.out.println("Exactly same: true");
        else
            System.out.println("Exactly same: false");*/
        System.out.printf("LRINIT TIME: %.2f secs\n", ellapsedTime/Math.pow(10, 9.0));
        return true;
    }

    /**
     * Used so as to cleanup the memory allocated to matrices in order to perform
     * linear regression.
     */
    public void terminateLR() { //throws SQLException {
        this.lrid = 0;
        this.X = null;
        this.y = null;
        this.XXT = null;
        this.Xy = null;
        this.w = null;
        this.rows = -1;
        this.finished = true;
        this.exactlySame = false;
        if (this.plan != null)
            this.plan.clear();
        this.plan = null;
        System.gc();
    }

    /**
     * Consults table linearregres in order to find if we have already performed a linear regression in this dataset
     * for these specific attributes.
     *
     * @param  conn        Current connection with the database.
     * @param  did         The dataset id.
     * @param  features    The features upon which we would like to execute linear regression.
     * @param  target      The value we would like to approximate.
     */
    public void seekLinearRegression(Connection conn, long did, Structs.featvalueMap[] features, Structs.featvalueMap target) 
           throws SQLException, ClassNotFoundException {
        long startTime = System.nanoTime();
        PreparedStatement pstmt = null;
        ResultSet rs = null; //We need result set so as to get the lrid in case for updating linearregres table.
        boolean success = false;
        StringBuffer sbf = null; 
        String feats = null;
        int lrD = features.length + 1;
        try {
            sbf = new StringBuffer();
            for (int i = 0; i < features.length; i++) {
                sbf.append(features[i].feature);
                sbf.append("|");
            }
            Class.forName("com.mysql.jdbc.Driver");
            pstmt = conn.prepareStatement(this.slctLRQ);
            pstmt.setLong(1, did); pstmt.setString(2, target.feature);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                feats = rs.getString(2);
                if (feats.compareTo(sbf.toString()) == 0) {
                    this.lrid = rs.getLong(1);
                    break;
                }
            }
            //System.out.println("LRID: " + this.lrid);
            rs.close();
            pstmt.close();
            if (this.OUTPUT)
                System.out.printf("SEEK LR: %.4f\n", (System.nanoTime() - startTime)/Math.pow(10,9.0));
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        }
    }

    /**
     * Produces matrix XX^T and vector Xy from accessing the points stored in disk directly.
     *
     * @param  conn      Connection with the database.
     * @param  did       Desired dataset's id.
     * @param  initD     Number of dimensions of the initial relation.
     * @param  features  The features we are taking into account.
     * @param  target    The target observation we would like to approximate.
     * @param  start     Start point of the non-estimated interval.
     * @param  end       End point of the non-estimated interval.
     * @param  act       Action to perform upon estimating matrix XXT and vector Xy 
     */
    private void generateMV(Connection conn, long did, int initD, Structs.featvalueMap[] features, Structs.featvalueMap target, 
            long start, long end, Structs.Action act, long[] times) throws SQLException, ClassNotFoundException {
        // Establish connection with `data` relation in order 
        // to retrive the points belonging in user's query.
        //System.out.println("GENERATE MV");
        //System.out.println("Number of rows: " + this.rows);
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        byte[] values = new byte[initD * SIZE_OF_DOUBLE];
        byte[] value = new byte[SIZE_OF_DOUBLE];
        int count = 0;
        double v = 0.0;
        boolean hasMore = true;
        DoubleMatrix tempM = DoubleMatrix.zeros(features.length + 1, features.length + 1);
        DoubleMatrix tempy = DoubleMatrix.zeros(features.length + 1);
        long queryTime = 0, mmultTime = 0, mmultDuration = 0;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            pstmt = conn.prepareStatement(this.slctDataQ, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            pstmt.setFetchSize(Integer.MIN_VALUE);
            pstmt.setLong(1, start);
            pstmt.setLong(2, end);
            pstmt.setLong(3, did);
            queryTime = System.nanoTime();
            rs = pstmt.executeQuery();
            times[0] += (System.nanoTime() - queryTime);
            while (hasMore) {
                //System.out.println("CHUNK");
                count = 0;
                queryTime = System.nanoTime();
                while (count < this.rows && rs.next()) { // Loop that gathers the data values in steps
                    System.arraycopy(rs.getBlob(1).getBytes(1, SIZE_OF_DOUBLE*initD), 0, values, 0, initD*SIZE_OF_DOUBLE);
                    for (int i = 0; i < features.length; i++) {
                        System.arraycopy(values,features[i].valueId*SIZE_OF_DOUBLE,value,0,SIZE_OF_DOUBLE);
                        v = ByteBuffer.wrap(value).getDouble();
                        this.X.put(count, i, v);
                    }
                    this.X.put(count, features.length, 1.0);
                    System.arraycopy(values,target.valueId*SIZE_OF_DOUBLE,value,0,SIZE_OF_DOUBLE);
                    v = ByteBuffer.wrap(value).getDouble();
                    this.y.put(count, v);
                    /*if (count % 100000 == 0) {
                        System.out.println("Count: " + count);
                        System.out.println("HasMore: " + hasMore);
                    }*/
                    count++;
                }
                times[0] += (System.nanoTime() - queryTime);
                // Debugging purposes
                /*System.out.println("Matrix X");
                for (int i = 0; i < count; i++) {
                    for (int j = 0; j < features.length; j++)
                        System.out.printf("%.6f\t", this.X.get(i,j));
                    System.out.printf("%.4f\n", this.X.get(i,features.length));
                }*/
                mmultTime = System.nanoTime();
                if (count != this.rows) { // Except from the fact that we need to stop external loop
                                          // we also need to to perform the operations in smaller matrices.
                    if (count != 0) {     // Case under which we have read at least one tuple from relation 'data'.
                        for (int i = count; i < this.rows; i++) {
                            this.y.put(i, 0);
                            for (int j = 0; j < features.length + 1; j++)
                                this.X.put(i, j, 0);
                        }
                    } else {                // The input stream has been exhausted for sure...
                        hasMore = false;
                    }
                    System.gc();
                } 
                if (hasMore) {
                    (this.X.transpose()).mmuli(this.X, tempM); // X^T x X multiplication
                    (this.X.transpose()).mmuli(this.y, tempy); // X^T x y multiplication
                }
                if (hasMore && act == Structs.Action.subtract) {
                    //System.out.println("***SUBTRACTION***");
                    tempM.muli(-1.0);
                    tempy.muli(-1.0);
                }
                if (hasMore) {
                    this.XXT.addi(tempM);
                    this.Xy.addi(tempy);
                }
                times[1] += (System.nanoTime() - mmultTime);
            }
            System.gc();
        } finally {
            if (rs != null) { rs.close(); rs = null; }
            if (pstmt != null) { pstmt.close(); pstmt = null; }
        }
    } 

    /**
     * Retrieves already estimated matrix XX^T and vector Xy from relation matrix_vector.
     *
     * @param  conn      Connection with the database.
     * @param  pn        Policy Node.
     * @param  d         Dimensionallity current interval (it is not the augmented one).
     */
    private void retrieveMV(Connection conn, Structs.policyNode pn, int d, long[] times) throws SQLException, ClassNotFoundException {
        // Establish connection with `matrix_vector` relation  
        //System.out.println("RETRIEVE MV");
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int lrD = d + 1;
        byte[] byteVec = new byte[lrD*SIZE_OF_DOUBLE];
        byte[] byteM = new byte[lrD*lrD*SIZE_OF_DOUBLE];
        byte[] value = new byte[SIZE_OF_DOUBLE];
        DoubleMatrix tempM = DoubleMatrix.zeros(lrD, lrD);
        DoubleMatrix tempy = DoubleMatrix.zeros(lrD);
        long startTime = 0;
        //System.out.println("Dimensions: " + (lrD));
        try {
            startTime = System.nanoTime();
            if (!pn.cached) {
                System.out.printf("WRONG");
                Class.forName("com.mysql.jdbc.Driver");
                pstmt = conn.prepareStatement(this.slctMVQ);
                pstmt.setLong(1, pn.iid);
                rs = pstmt.executeQuery();
                rs.next();
                System.arraycopy(rs.getBlob(1).getBytes(1, SIZE_OF_DOUBLE*lrD*lrD), 0, byteM, 0, lrD*lrD*SIZE_OF_DOUBLE);
                System.arraycopy(rs.getBlob(2).getBytes(1, SIZE_OF_DOUBLE*lrD), 0, byteVec, 0, lrD*SIZE_OF_DOUBLE);
                //System.out.println("TEMPM OVERALL");
                for (int i = 0; i < lrD; i++) {
                    System.arraycopy(byteVec, i*SIZE_OF_DOUBLE, value, 0, SIZE_OF_DOUBLE); //Vector Xy
                    tempy.put(i, ByteBuffer.wrap(value).getDouble());
                    for (int j = 0; j < lrD; j++) {
                        System.arraycopy(byteM, (i*lrD+j)*SIZE_OF_DOUBLE, value, 0, SIZE_OF_DOUBLE); //Matrix XXT
                        tempM.put(i, j, ByteBuffer.wrap(value).getDouble());
                        //System.out.printf("%.5f, ", ByteBuffer.wrap(value).getDouble());
                    }
                    //System.out.printf("\n");
                }
                //System.out.println("----");
                //DEBUG INFO:
                /*System.out.println("tempM Diag:");
                for (int i = 0; i < d + 1; i++)
                    System.out.printf("%.6f\t", tempM.get(i,i));
                System.out.printf("\n");*/
                //
            } else {
                for (int i = 0; i < lrD; i++) {
                    System.arraycopy(pn.Xy, i*SIZE_OF_DOUBLE, value, 0, SIZE_OF_DOUBLE); //Vector Xy
                    tempy.put(i, ByteBuffer.wrap(value).getDouble());
                    for (int j = 0; j < lrD; j++) {
                        System.arraycopy(pn.XXT, (i*lrD+j)*SIZE_OF_DOUBLE, value, 0, SIZE_OF_DOUBLE); //Matrix XXT
                        tempM.put(i, j, ByteBuffer.wrap(value).getDouble());
                        //System.out.printf("%.5f, ", ByteBuffer.wrap(value).getDouble());
                    }
                    //System.out.printf("\n");
                }
            }
            this.XXT.addi(tempM);
            this.Xy.addi(tempy);
            times[1] += (System.nanoTime() - startTime);
            System.gc();
        } finally {
            if (rs != null) { rs.close(); rs = null; }
            if (pstmt != null) { pstmt.close(); pstmt = null; }
        }
    }

    /**
     * Retrieves already estimated coefficints from the desired interval (after the suggestion of optimizer).
     *
     * @param  conn      Connection with the database.
     * @param  iid       Interval's id.
     * @param  d         Dimensionallity current interval (it is not the augmented one).
     */
    private void retrieveCoeffs(Connection conn, long iid, int d) throws SQLException, ClassNotFoundException {
        // Establish connection with `matrix_vector` 
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        byte[] byteVec = new byte[(d+1)*SIZE_OF_DOUBLE];
        byte[] value = new byte[SIZE_OF_DOUBLE];
        this.w = DoubleMatrix.zeros(d + 1);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            pstmt = conn.prepareStatement(this.slctMVQ_coeff);
            pstmt.setLong(1, iid);
            rs = pstmt.executeQuery();
            rs.next();
            System.arraycopy(rs.getBlob(1).getBytes(1, SIZE_OF_DOUBLE*(d+1)), 0, byteVec, 0, (d+1)*SIZE_OF_DOUBLE);
            for (int i = 0; i < (d+1); i++) {
                System.arraycopy(byteVec, i*SIZE_OF_DOUBLE, value, 0, SIZE_OF_DOUBLE); //Vector Xy
                this.w.put(i, ByteBuffer.wrap(value).getDouble());
            }
            System.gc();
        } finally {
            if (rs != null) { rs.close(); rs = null; }
            if (pstmt != null) { pstmt.close(); pstmt = null; }
        }
    }

    /**
     * Estimates matrix XX^T and vector Xy that form the linear system from which the coeffs are going
     * to be estimated.
     *
     * @param  conn      Connection with the database.
     * @param  did       Desired dataset's id.
     * @param  initD     The number of dimensions for the initial matrix.
     * @param  features  The features we are taking into account.
     * @param  target    The target observation we would like to approximate.
     * @return           Boolean value that indicates whether the generated XX^T matrix and Xy vector
     *                   are produced without any connection error. 
     */
    private boolean executeLR(Connection conn, long did, int initD, Structs.featvalueMap[] features, Structs.featvalueMap target) {
        //Parse the plan.
        Structs.policyNode pn;
        //Structs.RawInterval rIntv;
        boolean success = false;
        //long startTime = 0;
        //long endTime = 0;
        long diskAccesses = 0;
        long[] times = new long[2]; times[0] = 0; times[1] = 0; //Database Time - MMULTI Time
        try {
            //startTime = System.nanoTime();
            for (int i = 0; i < this.plan.size(); i++) { 
                pn = this.plan.get(i);
                switch(pn.action) {
                    case create:
                    case subtract:
                        diskAccesses++;
                        this.generateMV(conn, did, initD, features, target, pn.start, pn.end, pn.action, times);
                        System.gc();
                        break;
                    case fetch:  // Fetch already estimated matrices and vectors.
                        if (this.exactlySame)
                            this.retrieveCoeffs(conn, pn.iid, features.length); //retrieve only the already estimated coeffs
                        else
                            this.retrieveMV(conn, pn, features.length, times); //retrieve XXT and Xy
                        System.gc();
                        break;
                }
            }
            if (this.OUTPUT) {
                System.out.printf("COMBINED INTERVALS: %4d\n", this.plan.size());
                System.out.printf("QUERY TIME: %.2f secs - #Accesses: %d\n", times[0]/Math.pow(10, 9.0), diskAccesses);
                System.out.printf("MMULT TIME: %.2f secs\n", times[1]/Math.pow(10, 9.0));
            }
            success = true;
        } catch(ClassNotFoundException e) {
            System.err.println("LR: Error accessing raw datapoints!");
            e.printStackTrace();
        } catch(SQLException e) {
            System.err.println("LR: Error accessing raw datapoints!");
            e.printStackTrace();
        }
        return success; 
    }
    
    /**
     * Stores the results of the successful linear regression execution. 
     *
     * @param  conn       Current connection established with the database.
     * @param  did        Desired dataset's id.
     * @param  features   Features/Measurements upon which we execute Linear Regression.
     * @param  target     Observation that we would like to approximate.
     * @param  tmsp       Start point for the linear regression query to be executed.
     * @param  range      Range of the linear regression query to be executed.
     *
     * @throws ClassNotFoundException, SQLException
     */
    private void storeResults(Connection conn, long did, Structs.featvalueMap[] features, Structs.featvalueMap target, 
            long tmsp, long range) throws ClassNotFoundException, SQLException { 
        PreparedStatement pstmt = null;
        ResultSet rs = null; //We need result set so as to get the lrid in case for updating linearregres table.
        boolean success = false;
        StringBuffer sbf = null; 
        int lrD = features.length + 1;
        byte[] value = new byte[SIZE_OF_DOUBLE];
        byte[] Mvalues = new byte[lrD*lrD*SIZE_OF_DOUBLE]; 
        byte[] Vvalues = new byte[lrD*SIZE_OF_DOUBLE];
        byte[] Wvalues = new byte[lrD*SIZE_OF_DOUBLE];
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn.setAutoCommit(false);
            if (this.lrid == 0) { // We need to update 'linearregres' relation as well.
                pstmt = conn.prepareStatement(this.insrtLRQ, Statement.RETURN_GENERATED_KEYS);
                pstmt.setLong(1, did); pstmt.setInt(2, features.length + 1); pstmt.setString(4, target.feature); 
                sbf = new StringBuffer();
                for (int i = 0; i < features.length; i++) { //WARNING: There always exists one additional dimension.
                                                            //Notion of augmented vectors.
                    sbf.append(features[i].feature);
                    sbf.append("|");
                }
                pstmt.setString(3, sbf.toString());
                pstmt.executeUpdate();
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    this.lrid = rs.getLong(1);
                } else {
                    System.err.println("Foreign key violation!");
                    throw new SQLException();
                }
                rs.close();
            }
            //Now we are ready to store matrix XXT along with vectors Xy and w. 
            pstmt = conn.prepareStatement(this.insrtMVQ);
            pstmt.setLong(1, did); pstmt.setLong(2, this.lrid); pstmt.setLong(3, tmsp); pstmt.setLong(4, range);
            //System.out.println("OVERALL XXT MATRIX:");
            for (int i = 0; i < lrD; i++) {
                ByteBuffer.wrap(value).putDouble(this.w.get(i)); //Weight coefficients.
                System.arraycopy(value, 0, Wvalues, i*SIZE_OF_DOUBLE, SIZE_OF_DOUBLE); 
                ByteBuffer.wrap(value).putDouble(this.Xy.get(i)); //Vector Xy
                System.arraycopy(value, 0, Vvalues, i*SIZE_OF_DOUBLE, SIZE_OF_DOUBLE); 
                for (int j = 0; j < lrD; j++) {
                    ByteBuffer.wrap(value).putDouble(this.XXT.get(i,j)); //Matrix elements.
                    System.arraycopy(value, 0, Mvalues, (i*lrD+j)*SIZE_OF_DOUBLE, SIZE_OF_DOUBLE); 
                    //System.out.printf("%.6f, ", ByteBuffer.wrap(value).getDouble());
                }
                //System.out.printf("\n");
            }
            //System.out.println("");
            //DEBUG INFO:
            /*System.out.println("Stored XXT Diag:");
            for (int i = 0; i < lrD; i++)
                System.out.printf("%.6f\t", this.XXT.get(i,i));
            System.out.printf("\n");*/
            pstmt.setBytes(5, Mvalues); pstmt.setBytes(6, Vvalues); pstmt.setBytes(7, Wvalues);
            pstmt.executeUpdate();
            conn.commit();
            pstmt.close();
            success = true;
        } catch(ClassNotFoundException e) {
            System.err.println("LR: Error storing results!");
            e.printStackTrace();
        } catch(SQLException e) {
            System.err.println("LR: Error storing results!");
            e.printStackTrace();
        } finally { 
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
            if (!success && conn != null)
                conn.rollback();
            else if (conn != null)
                conn.setAutoCommit(true);
            else
                conn = null;
        }
    }

    /**
     * Linear Regression Driver function.
     *
     * @param  conn       Current connection established with the database.
     * @param  did        Desired dataset's id.
     * @param  initD      The number of dimensions of the initial dataset.
     * @param  features   Features/Measurements upon which we execute Linear Regression.
     * @param  target     Observation that we would like to approximate.
     * @param  tmsp       Start point for the linear regression query to be executed.
     * @param  range      Range of the linear regression query to be executed.
     * @param  opt        Boolean parameter for setting on or off the optimizer.
     * @param  store      Boolean parameter indicating whether we are going to store results back to disk.
     *
     * @throws ClassNotFoundException, SQLException
     */
    public double[] linearregress(Connection conn, long did, int initD, Structs.featvalueMap[] features, Structs.featvalueMap target, 
            long tmsp, long range, boolean opt, boolean store) { // throws ClassNotFoundException, SQLException {
        double[] coeffs = null;
        long storeTime = 0;
        try {
            this.seekLinearRegression(conn, did, features, target);
            //System.out.println("Linear Regression ID: " + this.lrid);
            if (!this.initializeLR(conn, did, initD, features.length, tmsp, range, opt)) {
                System.err.println("Aborting LR execution!");
            }
            this.executeLR(conn, did, initD, features, target);
            //DEBUG INFO:
            /*System.out.println("Xy:");
            for (int i = 0; i < features.length + 1; i++) 
                System.out.printf("%.6f\t", this.Xy.get(i));
            System.out.printf("\n");
            System.out.println("XXT Diag:");
            for (int i = 0; i < features.length + 1; i++)
                System.out.printf("%.6f\t", this.XXT.get(i,i));
            System.out.printf("\n");*/
            //estimate coefficients
            if (!this.exactlySame) //no need to do that for an LR interval that already exists and is suggested by the Optimizer
                this.w = Solve.solvePositive(this.XXT, this.Xy);
            /*System.out.println("coeff");
            for (int i = 0; i < features.length + 1; i++) 
                System.out.printf("%.6f\t", this.w.get(i));
            System.out.printf("\n");*/
            //store LR result back to disk and store XXT matrix along with Xy vector...
            if (store && !this.exactlySame) {
                //System.out.printf("Range: %d\n", range);
                storeTime = System.nanoTime();
                this.storeResults(conn, did, features, target, tmsp, range);
                if (this.OUTPUT)
                    System.out.printf("STORE TIME: %.2f\n", (System.nanoTime() - storeTime)/Math.pow(10,9.0));
            }
        } catch(ClassNotFoundException e) {
            System.err.println("Query could not be processed!");
            e.printStackTrace();
        } catch(SQLException e) {
            System.err.println("Query could not be processed!");
            e.printStackTrace();
        } /* finally {
            this.terminateLR();
        } */
        return coeffs;
    }

}
