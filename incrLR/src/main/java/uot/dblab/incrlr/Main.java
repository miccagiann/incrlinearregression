package uot.dblab.incrlr;

import java.util.Random;
import java.util.TreeSet;
import java.util.ArrayList;
import java.sql.*;

public class Main {
    
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Expected Input!");
            Main.menu();
            return;
        }
        try {
            switch (Integer.parseInt(args[0])) {
                case 1: //Load Dataset Use Case
                    if (args.length != 2) {
                        System.out.println("Expected filename!");
                        return;
                    } else {
                        Main.loadData(args[1]);
                    }
                    break;
                case 2: //Delete Dataset Use Case
                    Main.deleteData();
                    break;
                case 3: //Optimizer Benchmark Use Case
                    if (args.length != 8) {
                        System.out.println("No range for new intervals provided!");
                        Main.menu();
                        return;
                    } 
                    Main.avgQueryBenchmark(Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]), 
                            Integer.parseInt(args[4]), Integer.parseInt(args[5]), Double.parseDouble(args[6]), Integer.parseInt(args[7]) != 0);
                    break;
                case 4:
                    if (args.length != 7) {
                        System.out.println("No range for new intervals provided!");
                        Main.menu();
                        return;
                    } 
                    Main.dummyBenchmark(Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]), 
                            Integer.parseInt(args[4]), Integer.parseInt(args[5]), Integer.parseInt(args[6]));
                    break;
                case 5:
                    if (args.length != 5) {
                        System.out.println("No range for new intervals provided!");
                        Main.menu();
                        return;
                    }
                    Main.ovlapBenchmark(Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]));
                    break;
                default:
                    Main.menu();
                    break;
            }
        } catch (NumberFormatException e) {
            System.out.println("Wrong Input provided!");
            System.out.println("NumberFormatException");
            Main.menu();
        }
    }

    public static void menu() {
        System.out.println("***** -INCREMENTAL LR OPTIMIZER- *****");
        System.out.println("Options: ");
        System.out.printf("\tLoad Dataset Use Case   --> 1\n");
        System.out.printf("\tDelete Dataset Use Case --> 2\n");
        System.out.printf("\tOptimizer Benchmark     --> 3\n");
        System.out.println("--------------------------------------");
    }

    public static void loadData(String filename) {
        Backend b = new Backend();
        b.establishConnection("innodb", "root", "DBLab123");
        try {
            long start = System.nanoTime();
            b.loadDataset(filename, "test", null, new String[]{ "1","2","3","4","5","6","7","8","9","10" }, ',');
            long end = System.nanoTime();
            System.out.printf("Load Dataset Exe Time: %.6f secs\n", (end - start)/Math.pow(10,9.0));
        } catch (Exception e) {
            System.err.println("Populating Database has failed!");
            e.printStackTrace();
        }
        b.closeConnection();
    }

    public static void deleteData() {
        Backend b = new Backend();
        b.establishConnection("innodb", "root", "DBLab123");
        try {
            long start = System.nanoTime();
            b.deleteDataset("test");
            long end = System.nanoTime();
            System.out.printf("Delete Dataset Exe Time: %.6f secs\n", (end - start)/Math.pow(10,9.0));
        } catch (Exception e) {
            System.err.println("Deleting Data has failed!");
            e.printStackTrace();
        }
        b.closeConnection();
    }

    /**
     * It is a specific benchmark that records the average time of executing a query in our dataset when already estimated intervals that cover
     * a specific number of datapoints of our initial dataset exist. Coverage of the existing dataset can cumulate between 0% to 100% percent
     * and we allow this experiment to be executed multiple times as we cover more and more data points of the original dataset. A coverage
     * step of 5% denotes that we are going to execute this benchmark 20 times ideally for the following coverage levels: [0%, 5%, 10%, ..., 95%].
     *
     * @param  dStart             The id of the starting point of the existing dataset.
     * @param  datasetSize        The actual data set's size.
     * @param  minRange           The minimum range of each one of the generated intervals (already estimated queries).
     * @param  maxRange           The maximum range of each one of the generated intervals (already estimated queries).
     * @param  numOfQueries       The number of queries to be executed at each coverage level (these are not already estimated queries and can 
     *                            extend to any consecutive data points belonging to the initial dataset).
     * @param  coverStep          The coverage step.
     * @param  opt_on             Execute this query using the optimizer.
     */
    public static void avgQueryBenchmark(int dStart, int datasetSize, int minRange, int maxRange, int numOfQueries, double coverStep, boolean opt_on) {
        Backend b = new Backend();
        b.establishConnection("innodb", "root", "DBLab123");
        TreeSet<Structs.policyNode> intvSet = new TreeSet<Structs.policyNode>();
        ArrayList<Structs.policyNode> queries = new ArrayList<Structs.policyNode>(numOfQueries);
        Random gen = new Random(64);
        int currQStartId = 0, currQRange = 0;
        long totalOptExeTime = 0, totalNonOptExeTime = 0, beginExe = 0, nonOptDiff = 0, optDiff = 0;
        double currCover = 0.0, avgOptExeTime = 0.0, avgNonOptExeTime = 0.0;
        final double THRESHOLD = 96.0;
        System.out.println("PARAMETERS: ");
        System.out.printf("Start = %d, Dataset Size = %d, MinRange = %d, MaxRange = %d, #Queries = %d, CoverStep = %.2f%%\n",
                dStart, datasetSize, minRange, maxRange, numOfQueries, coverStep);
        System.out.println("");
        System.out.println("-----CREATING QUERIES-----");
        for (int i = 0; i < numOfQueries; i++) {
            currQStartId = dStart + gen.nextInt(datasetSize - dStart + 1);
            currQRange = minRange + gen.nextInt(maxRange - minRange);
            if (currQRange + currQStartId - 1 > datasetSize)
                currQRange = gen.nextInt(datasetSize - currQStartId + 1);
            //currQRange = minRange + gen.nextInt(datasetSize - currQStartId -  + 1); //TODO: NEEDS TO BE ERASED...
            queries.add(new Structs.policyNode(currQStartId, currQStartId + currQRange - 1));
            System.out.println("Query ID: " + (i+1) + " --- " + "Start: " + currQStartId + " --- " + "Range: " + currQRange);
        }
        try {
            if (opt_on) { //OPTIMIZER IS ON
                do {
                    System.out.println("==========================");
                    //Execute the queries and gather average times...
                    b.OUTPUT = true;
                    totalOptExeTime = 0;
                    totalNonOptExeTime = 0;
                    int i = 0;
                    for (Structs.policyNode pn: queries) {
                        i++;
                        System.out.println("OPTIMIZER ON");
                        System.out.println("#" + i);
                        beginExe = System.nanoTime();
                        b.linearregress("test", new String[]{"1","2","3","4","5","6","7","8","9"}, "10", pn.start, pn.end - pn.start + 1, true, false);
                        optDiff = (System.nanoTime() - beginExe);
                        System.out.printf("Optimized Exe time: %.2f\n", optDiff/Math.pow(10,9.0));
                        totalOptExeTime += optDiff;
                    }
                    avgOptExeTime = (totalOptExeTime/Math.pow(10,9.0))/queries.size();
                    avgNonOptExeTime = (totalNonOptExeTime/Math.pow(10,9.0))/queries.size();
                    System.out.printf("#Intervals: %d, Coverage: %.2f%%, OptExeTime: %.6f, NonOptExeTime: %.6f\n", intvSet.size(), currCover, avgOptExeTime, avgNonOptExeTime);
                    System.out.println("-----MATERIALIZING INTERVALS-----");
                    if (THRESHOLD - currCover <= coverStep) {
                        coverStep = THRESHOLD - currCover;
                        System.err.println("Cover Step Changed: " + coverStep);
                    }
                    currCover = Main.createIntervals(intvSet, dStart, datasetSize, minRange, maxRange, coverStep, currCover, gen, b);
                    System.out.println("-----END OF MATERIALIZATION-----");
                    System.out.printf("Coverage: %.2f\n", currCover);
                } while(currCover < THRESHOLD);
                System.out.println("==========================");
                //Execute the queries and gather average times for the last time...
                totalOptExeTime = 0;
                totalNonOptExeTime = 0;
                b.OUTPUT = true;
                for (Structs.policyNode pn: queries) {
                    System.out.println("OPTIMIZER ON");
                    beginExe = System.nanoTime();
                    b.linearregress("test", new String[]{"1","2","3","4","5","6","7","8","9"}, "10", pn.start, pn.end - pn.start + 1, true, false);
                    optDiff = (System.nanoTime() - beginExe);
                    System.out.printf("Optimized Exe time: %.2f\n", optDiff/Math.pow(10,9.0));
                    totalOptExeTime += optDiff;
                }
            } else { //OPTIMIZER OFF
                for (Structs.policyNode pn: queries) {
                    System.out.println("==========================");
                    System.out.println("OPTIMIZER OFF");
                    b.OUTPUT = false;
                    beginExe = System.nanoTime();
                    b.linearregress("test", new String[]{"1","2","3","4","5","6","7","8","9"}, "10", pn.start, pn.end - pn.start + 1, false, false);
                    nonOptDiff = (System.nanoTime() - beginExe);
                    System.out.printf("Non-optimized Exe time: %.2f\n", nonOptDiff/Math.pow(10,9.0));
                    totalNonOptExeTime += nonOptDiff;
                }
            }
            avgOptExeTime = (totalOptExeTime/Math.pow(10,9.0))/queries.size();
            avgNonOptExeTime = (totalNonOptExeTime/Math.pow(10,9.0))/queries.size();
            System.out.printf("#Intervals: %d, Coverage: %.2f%%, OptExeTime: %.6f, NonOptExeTime: %.6f\n", 
                    intvSet.size(), currCover, avgOptExeTime, avgNonOptExeTime);
            System.out.println("==========================");
            //Finally print all the intervals existing in disk
            System.out.println("===MATERIALIZED QUERIES===");
            for (Structs.policyNode pn: intvSet)
                System.out.printf("[%d,%d] --- %d\n", pn.start, pn.end, pn.end-pn.start+1);
            System.out.println("==========================");
            System.out.println("=====END OF BENCHMARK=====");
        } catch (Exception e) {
            System.err.println("Benchmarking has failed!");
            e.printStackTrace();
        }
        b.closeConnection();
    }

    public static double createIntervals(TreeSet<Structs.policyNode> intvSet, int dStart, int datasetSize, int minRange, int maxRange, double coverStep, double currCover, Random gen, Backend b) throws SQLException, ClassNotFoundException {
        double prevCover = currCover;
        int currQStartId = 0, currQRange = 0;
        b.OUTPUT = false;
        while (currCover - prevCover < coverStep) {
            currQStartId = dStart + gen.nextInt(datasetSize - dStart + 1);
            currQRange = minRange + gen.nextInt(maxRange - minRange + 1);
            if (currQRange + currQStartId - 1 > datasetSize)
                currQRange = gen.nextInt(datasetSize - currQStartId + 1);
            b.linearregress("test", new String[]{"1","2","3","4","5","6","7","8","9"}, "10", currQStartId, currQRange, true, true);
            intvSet.add(new Structs.policyNode(currQStartId, currQStartId + currQRange - 1));
            currCover = Main.findCoverage(intvSet, dStart, datasetSize);
        }
        return currCover;
    }

    public static double findCoverage(TreeSet<Structs.policyNode> intvSet, long dStart, long datasetSize) {
        long prevEnd = dStart, minStart = 0, maxEnd = 0, currCover = 0;
        long datasetEndId = dStart + datasetSize - 1;
        for (Structs.policyNode pn: intvSet) {
            if (prevEnd < pn.start) { //disjoint intervals
                currCover += prevEnd - minStart + 1;
                minStart = pn.start;
            }
            prevEnd = Math.max(prevEnd, pn.end);
            if (prevEnd > datasetSize) {
                prevEnd = datasetEndId;
                break;
            }
        }
        currCover += prevEnd - minStart + 1;
        return (currCover/(datasetSize*1.0)) * 100;
    }


    /**
     * Perform a specific benchmark upon the dataset stored in MySQL. At each time the user chooses
     * among creating a newly estimated interval, calculating a query with the help of optimizer or 
     * calculating a query without the optimizer being activated. Used for cases that we want to simulate
     * cold buffer pool.
     *
     * @param  datasetSize        The actual datasize.
     * @param  querySize          The size of the query. We always start from the very first point of the dataset.
     * @param  n                  The number of intervals to be generated.
     * @param  startId            The beginning point of all the intervals to be generated.
     * @param  intvRange          The maximum range of each one of the generated intervals.
     * @param  option             Separate between creating interval, estimating non-optimized query, estimating optimized query.
     */
    public static void dummyBenchmark(int datasetSize, int querySize, int n, int startId, int intvRange, int option) {
        Backend b = new Backend();
        b.establishConnection("innodb", "root", "DBLab123");
        long beginExe = 0; long endExe = 0;
        double nonOptExeTime = 0;
        double optExeTime = 0;
        int count = 0;
        Random gen = new Random(64);
        try {
            while(count < n) {
                if (option == 1)
                    b.linearregress("test", new String[]{"1","2","3","4","5","6","7","8","9"}, "10", startId, intvRange, true, true);
                else if (option == 2) {
                    beginExe = System.nanoTime();
                    //Invoke optimized version
                    b.linearregress("test", new String[]{"1","2","3","4","5","6","7","8","9"}, "10", startId, querySize, true, false);
                    endExe = System.nanoTime();
                    optExeTime = (endExe - beginExe)/Math.pow(10,9.0);
                    System.out.printf("OptExeTime: %.6f secs\n", optExeTime);
                } else if (option == 3) {
                    beginExe = System.nanoTime();
                    //Run unoptimized version
                    b.linearregress("test", new String[]{"1","2","3","4","5","6","7","8","9"}, "10", startId, querySize, false, false);
                    endExe = System.nanoTime();
                    nonOptExeTime = (endExe - beginExe)/Math.pow(10,9.0);
                    System.out.printf("nonOptExeTime: %.6f secs\n", nonOptExeTime);
                }
                count++;
                System.out.printf("#Intervals: %d, OptExeTime: %.6f, NonOptExeTime: %.6f\n", count, optExeTime, nonOptExeTime);
            }
        } catch (Exception e) {
            System.err.println("Benchmarking has failed!");
            e.printStackTrace();
        }
        b.closeConnection();
    }

    /**
     * Benchmark creating concecutive overlapping intervals...
     *
     * @param  datasetSize       The size of the dataset.
     * @param  querySize         The size of the query [it always starts from the point with id = 1]
     * @param  n                 Number of intervals to be created.
     * @param  startId           The point id from which we allow estimated intervals to start.
     * @param  intvRange         The maximum range of estimated intervals [The minimum range is (1/4)*intvRangel.
     */
    public static void ovlapBenchmark(int datasetSize, int querySize, int startId, int intvRange) {
        Backend b = new Backend();
        b.establishConnection("innodb", "root", "DBLab123");
        long lrid = -1;
        long beginExe = 0; long endExe = 0;
        int prev_start = startId; int prev_end = startId;
        int curr_start = 0; int curr_end = 0;
        int curr_intvRange = 0;
        double nonOptExeTime = 0;
        double optExeTime = 0;
        double cover = 0.0;
        int count = 0;
        Random gen = new Random(64);
        try {
            //while(count < n) {
            while(cover < 100.0) {
                System.out.println("==========================");
                curr_start = prev_start + gen.nextInt(prev_end - prev_start + 1);
                curr_intvRange = (int)Math.round(0.25*intvRange + 0.75*gen.nextInt(intvRange));
                curr_end = curr_start + curr_intvRange - 1;
                if (curr_end > datasetSize) { //Make it dataset size...
                    curr_end = datasetSize;
                    curr_intvRange = curr_end - curr_start + 1;
                }
                b.linearregress("test", new String[]{"1","2","3","4","5","6","7","8","9"}, "10", curr_start, curr_intvRange, true, true);
                beginExe = System.nanoTime();
                //Invoke optimized version
                lrid = b.linearregress("test", new String[]{"1","2","3","4","5","6","7","8","9"}, "10", startId, querySize, true, false);
                endExe = System.nanoTime();
                optExeTime = (endExe - beginExe)/Math.pow(10,9.0);
                System.out.printf("OptExeTime: %.6f secs\n", optExeTime);
                beginExe = System.nanoTime();
                //Run unoptimized version
                b.linearregress("test", new String[]{"1","2","3","4","5","6","7","8","9"}, "10", startId, querySize, false, false);
                endExe = System.nanoTime();
                nonOptExeTime = (endExe - beginExe)/Math.pow(10,9.0);
                System.out.printf("nonOptExeTime: %.6f secs\n", nonOptExeTime);
                count++;
                if (curr_end > prev_end)
                    prev_end = curr_end; //modify prev_end if it is smaller than curr_end...
                prev_start = curr_start; //curr_start is always bigger that prev_start
                cover = (prev_end*100.0)/querySize;
                System.out.printf("CURR_START: %d, CURR_END: %d, PREV_START: %d, PREV_END: %d, CURR_INTV: %d\n",curr_start, curr_end, 
                        prev_start, prev_end, curr_intvRange);
                System.out.printf("Linear Regression Id: %d\n", lrid);
                System.out.printf("#Intervals: %d, Coverage: %.2f%%, OptExeTime: %.6f, NonOptExeTime: %.6f\n", count, cover, optExeTime, nonOptExeTime);
                //b.deleteLR(lrid);
                //break;
            }
        } catch (Exception e) {
            System.err.println("Benchmarking has failed!");
            e.printStackTrace();
        }
        b.closeConnection();
    }

}
