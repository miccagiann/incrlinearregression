package uot.dblab.incrlr;

import java.sql.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.Arrays;

/**
 * Class that connects our application to the MySQL instance running locally in our 
 * computer. Before executing the application, make sure that MySQL Server is open.
 */
public class Backend 
{
    private static final int SIZE_OF_DOUBLE = 8;
    private static final String defaultSocket = "jdbc:mysql://127.0.0.1:3306/";
    private static final String insrtDataQ = "INSERT INTO `data` (`dataset_id`, `timestamp`, `values`) VALUES (?,?,?)";
    private static final String insrtDatasetQ = "INSERT INTO `datasets` (`name`, `d`, `description`, `features`) VALUES(?,?,?,?)";
    private static final String dltLRQ = "DELETE FROM `linearregres` WHERE `id` = ?";
    private static final String dltDatasetQ = "DELETE FROM `datasets` WHERE `name`=  ?";
    private static final String slctDatasetQ = "SELECT * FROM `datasets` WHERE `name` = ?";
    private static final int MAX = 10000;
    private String CONN;
    private Connection connect = null;

    public boolean OUTPUT;

    /**
     * Public constructor of class {@link #Backend}.
     */
    public Backend() {
        this.CONN = "jdbc:mysql://127.0.0.1:3306/innodb";
        this.OUTPUT = true;
        this.connect = null;
    }

    /**
     * Establishes or restablishes a connection to the desired database. In case of 
     * reestablishing a connection it is responsible to close safely the previous database
     * connection.
     *
     * @param  dbname    The name of the database that we are going to connect to.
     * @param  user      The name of the user accessing the database.
     * @param  password  The user's password.
     */
    public void establishConnection(String dbname, String user, String password) {
        boolean success = false;
        this.CONN = this.defaultSocket + dbname + "?user=" + user + "&password=" + password + "&rewriteBatchedStatements=true";
        try {
            if (this.connect != null) {
                this.connect.close();
                this.connect = null;
            }
            Class.forName("com.mysql.jdbc.Driver");
            this.connect = DriverManager.getConnection(this.CONN);
            success = true;
        } catch(ClassNotFoundException e) {
            System.err.println("JDBC DRIVER could not be resolved!");
            e.printStackTrace();
        } catch(NullPointerException e) {
            System.err.println("Connection could not be established!");
            e.printStackTrace();
        } catch(SQLException e) {
            System.err.println("Connection could not be established!");
            e.printStackTrace();
        } finally {
            if (!success) {
                this.connect = null;
            }
        }
    }

    /**
     * Closes an already existing connection. Otherwise, it doesn't do anything.
     */
    public void closeConnection() {
        try {
            if (this.connect != null) {
                this.connect.close();
                this.connect = null;
            }
        } catch(SQLException e) {
            System.err.println("Connection could not be closed!");
            e.printStackTrace();
        } finally {
            this.connect = null;
        }
    }

    //=============UNDER CONSTRUCTION=============
    /**
     * Loads the values stored in specific variables initialized by the user
     * into the R interface into the database. It combines all the values of these
     * variables together into a unique new dataset. If this combination of variables
     * is already used to form a dataset, then the old dataset is discarded.
     * 
     * @param  name         The name of the dataset.
     * @param  description  Optinal description for specific dataset.
     * @param  features     The names of the variables involved sorted in lexicographical order.
     * @param  values       The corresponding values of the variables.
     */
    /*public void loadDataset(String name, String description, String[] features,
            double[] values) {
    }*/
    //============================================

    /**
     * Loads the values stored in CSV txt file into a new dataset. It expects that the first
     * line contains information about the features and optionally the second line contains a 
     * description of the dataset.
     *
     * @param  filename     The name of the file from where the values of the new dataset are
     *                      going to be loaded.
     * @param delimeter     Delimeter for separating values.
     */
    public void loadDataset(String filename, String delimeter) {
    }

    /**
     * Exactly the same function as {@link #loadDataset} except the fact that information about
     * dataset's description and dataset's features are provided by the user and not being stored 
     * in the first two lines of CSV txt file.
     *
     * @param  filename     The name of the file where the values are stored.
     * @param  dsname       The name of the new dataset.
     * @param  description  Optinal description for specific dataset.
     * @param  features     The names of the variables involved sorted in lexicographical order.
     * @param  delimeter    Delimeter for separating values.
     *
     * @throws SQLException, ClassNotFoundException    In case that the rollback and the autocommit
     *                      operations fail (finally block).
     */
    public void loadDataset(String filename, String dsname, String description, String[] features, char delimeter) throws SQLException, ClassNotFoundException {
        if (features == null || dsname == null || filename == null) {
            System.err.println("Aborting Loading file: NULL parameters encountered!");
            return;
        }
        String line;
        StringBuffer sbf = new StringBuffer();
        BufferedReader br = null;
        TreeMap<String,Integer> feat_vals = new TreeMap<String,Integer>();
        int d = features.length;
        byte[] values = new byte[d*SIZE_OF_DOUBLE]; byte[] sortedvalues = new byte[d*SIZE_OF_DOUBLE];
        byte[] value = new byte[SIZE_OF_DOUBLE];
        int indx, pos, count;
        long did;
        long tmsp = 0;
        boolean success = false;
        Iterator<Integer> itr;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.connect.setAutoCommit(false);
            /* Creating the correct correspondance between sorted features and their corresponding values_ids... */
            count = 0;
            for (String f: features) {
                feat_vals.put(f, count); count++;
            }
            /* Insert new dataset */
            pstmt = this.connect.prepareStatement(this.insrtDatasetQ, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, dsname);
            pstmt.setInt(2, d);
            pstmt.setString(3, description);
            for (String f: feat_vals.keySet()) {
                sbf.append(f);
                sbf.append("|");
            }
            pstmt.setString(4, sbf.toString());
            pstmt.executeUpdate();
            rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
                did = rs.getLong(1);
            } else {
                System.err.println("Foreign key violation!");
                throw new SQLException();
            }
            rs.close();
            /* Insert new values */
            pstmt = this.connect.prepareStatement(this.insrtDataQ);
            br = new BufferedReader(new FileReader(filename));
            while ((line = br.readLine()) != null) {
                tmsp++;
                pos = 0;
                count = 0; 
                while ((indx = line.indexOf(delimeter, pos)) >= 0) {
                    //System.out.println("Value: " + Double.parseDouble(line.substring(pos, indx).trim()));
                    ByteBuffer.wrap(value).putDouble(Double.parseDouble(line.substring(pos, indx).trim()));
                    System.arraycopy(value, 0, values, count*SIZE_OF_DOUBLE, SIZE_OF_DOUBLE); 
                    pos = indx + 1;
                    count++;
                } //Do not forget last value as well...
                //System.out.println("Value: " + Double.parseDouble(line.substring(pos).trim()));
                ByteBuffer.wrap(value).putDouble(Double.parseDouble(line.substring(pos).trim()));
                System.arraycopy(value, 0, values, count*SIZE_OF_DOUBLE, SIZE_OF_DOUBLE); 
                pos = 0; itr = feat_vals.values().iterator();
                while (itr.hasNext()) { // Sort the values so as to correspond to the sorted sequence of features.
                    System.arraycopy(values, itr.next()*SIZE_OF_DOUBLE, sortedvalues, pos, SIZE_OF_DOUBLE);
                    pos += SIZE_OF_DOUBLE;
                }
                pstmt.setLong(1, did);
                pstmt.setLong(2, tmsp);
                pstmt.setBytes(3, sortedvalues);
                if (tmsp % this.MAX == 0) {
                    //System.out.printf("TMSP: %d\n", tmsp);
                    pstmt.executeBatch();
                    //pstmt.clearBatch();
                } 
                pstmt.addBatch();
            }
            pstmt.executeBatch();
            //pstmt.clearBatch();
            this.connect.commit();
            pstmt.close();
            br.close();
            success = true;
        } catch (IOException e) {
            System.err.println("Error reading input file!");
            e.printStackTrace();
        } catch(ClassNotFoundException e) {
            System.err.println("Connection could not be established!");
            e.printStackTrace();
        } catch(SQLException e) {
            System.err.println("Connection could not be established!");
            e.printStackTrace();
        }  catch(ArrayIndexOutOfBoundsException e) {
            System.err.println("Error with the dimensions!");
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
            if (!success && this.connect != null)
                this.connect.rollback();
            else if (this.connect != null)
                this.connect.setAutoCommit(true);
            else
                this.connect = null;
        }
    }

    /**
     * Deletes the user's specified dataset that exists in the database. If the dataset
     * does not exist, nothing happens. Our database uses InnoDB engine with cascade on 
     * update and delete operations upon foreign keys. This means that we need to delete
     * only the dataset located in the table datasets.
     *
     * @param dsname    The name of the dataset to be erased.
     *
     * @throws SQLException, ClassNotFoundException    In case that the rollback and the autocommit
     *                      operations fail (finally block).
     */
    public void deleteDataset(String dsname) throws SQLException, ClassNotFoundException {
        boolean success = false;
        PreparedStatement pstmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.connect.setAutoCommit(false);
            pstmt = this.connect.prepareStatement(this.dltDatasetQ);
            pstmt.setString(1, dsname);
            pstmt.executeUpdate();
            this.connect.commit();
            pstmt.close();
            success = true;
        } catch(ClassNotFoundException e) {
            System.err.println("Connection could not be established!");
            e.printStackTrace();
        } catch(SQLException e) {
            System.err.println("Connection could not be established!");
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
            if (!success && this.connect != null)
                this.connect.rollback();
            else if (this.connect != null)
                this.connect.setAutoCommit(true);
            else
                this.connect = null;
        }
    }

    /**
     * Deletes all the already estimated queries performed in a specific subset of
     * dataset's features (uses ON DELETE CASCADE policy in the INNODB engine).
     *
     * @param  lrid        The id of linear regression that we would like to delete.
     */
    public void deleteLR(long lrid) throws SQLException, ClassNotFoundException {
        boolean success = false;
        PreparedStatement pstmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.connect.setAutoCommit(false);
            pstmt = this.connect.prepareStatement(this.dltLRQ);
            pstmt.setLong(1, lrid);
            pstmt.executeUpdate();
            this.connect.commit();
            pstmt.close();
            success = true;
        } catch(ClassNotFoundException e) {
            System.err.println("Connection could not be established!");
            e.printStackTrace();
        } catch(SQLException e) {
            System.err.println("Connection could not be established!");
            e.printStackTrace();
        } finally {
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
            if (!success && this.connect != null)
                this.connect.rollback();
            else if (this.connect != null)
                this.connect.setAutoCommit(true);
            else
                this.connect = null;
        }
    }

    /**
     * Performs simple Linear Regression using the specified features as measurements and the feature 'target' as 
     * the target measurement. The user can perform linear regression upon a consecutive number of datapoints.
     *
     * @param  dsname        The name of the dataset.
     * @param  features      The measurement features used for linear regression.
     * @param  target        The target feature that we would like to approximate.
     * @param  tmsp          The start point of the query interval.
     * @param  range         The range of the query interval.
     * @param  opt           Turn the optimizer on/off.
     * @param  store         Store final result to disk.
     *
     * @return  The current's linear regression id.
     *
     * @throws SQLException, ClassNotFoundException    Error in the finally block...
     */
    public long linearregress(String dsname, String[] features, String target, long tmsp, long range, boolean opt, boolean store) 
        throws SQLException, ClassNotFoundException {
        if (dsname == null || features == null || target == null || tmsp <= 0) {
            System.err.println("Aborting Linear Regression: Wrong input parameters!");
            return -1;
        }
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        int initD = 0;
        long did = 0; long lrid = 0;
        String[] initFeats = null;
        Structs.featvalueMap[] featsValues = null;
        Structs.featvalueMap targetValue = null;
        int pos, count, indx;
        LinearRegression lr = new LinearRegression();
        lr.OUTPUT = this.OUTPUT;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            pstmt = this.connect.prepareStatement(this.slctDatasetQ);
            pstmt.setString(1, dsname);
            rs = pstmt.executeQuery();
            if (rs.next()) { // Extract information about the dataset.
                did = rs.getLong(1);
                initD = rs.getInt(3);
                String temp = rs.getString(5);
                initFeats = new String[initD];
                pos = 0; count = 0;
                indx = temp.indexOf('|', pos);
                while (indx >= 0 && indx <= temp.length() - 1) {
                    initFeats[count] = temp.substring(pos, indx);
                    pos = indx + 1; count++;
                    indx = temp.indexOf('|', pos);
                }
            } else {
                System.err.println("Dataset not found!");
                throw new SQLException();
            }
            rs.close();
            pstmt.close();
            // Verify that 'target' and 'features' belong in the dataset. 
            // Find the ids of their corresponding values.
            count = 0; // Represents the number of separate features encountered so far...
            pos = 0;
            Arrays.sort(features);
            featsValues = new Structs.featvalueMap[features.length];
            while (count != initFeats.length) { 
                if (initFeats[count].compareTo(target) == 0) { 
                    targetValue = new Structs.featvalueMap(initFeats[count], count); 
                } else if (pos < features.length && initFeats[count].compareTo(features[pos]) == 0) {
                    featsValues[pos] = new Structs.featvalueMap(initFeats[count], count);
                    pos++;
                    continue;
                }
                count++; 
            }
            if (pos != featsValues.length || targetValue == null) {
                System.err.println("Given features do not correspond with dataset's features!");
                throw new SQLException();
            }
            // Debugging purposes
            /*System.out.println("Target: " + targetValue.feature + ", " + targetValue.valueId);
            for (Structs.featvalueMap fv: featsValues)
                System.out.println("Feature: " + fv.feature + ", " + fv.valueId);*/
            // Ready to pass execution to LinearRegression Class.
            //long startTime = System.nanoTime();
            lr.linearregress(this.connect, did, initD, featsValues, targetValue, tmsp, range, opt, store);
            //System.out.printf("LRTIME: %.2f secs\n", (System.nanoTime() - startTime)/Math.pow(10, 9.0)); 
            lrid = lr.getLRID();
            long startTime = System.nanoTime();
            lr.terminateLR();
            System.out.printf("LRTERMINATE: %.2f secs\n", (System.nanoTime() - startTime)/Math.pow(10, 9.0));
        } catch(ClassNotFoundException e) {
            System.err.println("Connection could not be established!");
            e.printStackTrace();
        } catch(SQLException e) {
            System.err.println("Connection could not be established!");
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
                rs = null;
            }
            if (pstmt != null) {
                pstmt.close();
                pstmt = null;
            }
        }
        return lrid;
    }

}
