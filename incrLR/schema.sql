DROP TABLE `datasets`;
DROP TABLE `data`;
DROP TABLE `linearregres`;
DROP TABLE `matrix_vector`;

CREATE TABLE `datasets` (
    `id`          INT NOT NULL AUTO_INCREMENT PRIMARY KEY UNIQUE,
    `name`        VARCHAR(100) NOT NULL,
    `d`           INT NOT NULL,
    `description` VARCHAR(1000),
    `features`    TEXT NOT NULL
) ENGINE=INNODB;

CREATE TABLE `data` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,  
    `dataset_id`  INT NOT NULL, 
    `timestamp`   INT NOT NULL, 
    `values`      BLOB,
    CONSTRAINT FOREIGN KEY(`dataset_id`) REFERENCES `datasets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB;

/* Indicates if we have already performed a linear regression operation on the coresponding dataset. */
CREATE TABLE `linearregres` (
    `id`          INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `dataset_id`  INT NOT NULL,
    `d`           INT NOT NULL,
    `features`    TEXT NOT NULL,
    `target`      VARCHAR(100),
    FOREIGN KEY (`dataset_id`) REFERENCES `datasets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB;

CREATE TABLE `matrix_vector` (
    `id`          INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `dataset_id`  INT NOT NULL,
    `lr_id`       INT NOT NULL,
    `start`       INT NOT NULL,
    `range`       INT NOT NULL,
    `matrix`      BLOB,
    `vector`      BLOB,
    `coeffs`      BLOB,
    FOREIGN KEY (`lr_id`) REFERENCES `linearregres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`dataset_id`) REFERENCES `datasets` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB;

CREATE INDEX `datasets_indx`
ON `datasets` (`name`);

CREATE INDEX `data_indx`
ON `data` (`dataset_id`, `timestamp`);

CREATE INDEX `linearregres_indx`
ON `linearregres` (`dataset_id`, `target`, `features`(50));

CREATE INDEX `mv_start_indx`
ON matrix_vector (`dataset_id`, `lr_id`, `start`);

/*CREATE INDEX mv_end_indx
ON matrix_vector (start + range); */
